# Grand Tour Velo

This game makes you dive deep into the most famous cycling races, giving you the feeling of being inside a professional cycling team, reaching leadership of multiple contests: time, points, climbing, youngsters.

You will enjoy both the pleasure of making your own set of materials, by hand, and the threaling excitment of competing against opponents who are friends of yours.

Discover the rules and computational elements within the [EN](./EN) directory (to be translated form French actually) and proposed maps for *étapes* you can play wthin the [etapes](./etapes) directory. You may design your own maps and we are interested if you like to share them with the community (under an open source license, off course), so we could add them to the project.

A first prototype, made in 2018, gives a first eye on what the game looks like:

![](Prototype_2018.jpg)

And see what a running game looks like (basic rules):

TODO

This game is proposed to you thru an open source license (Licence Art Libre) which gives you the ability and rights to distribute it, modify and enhance it, if you respect the license and do not include any element under copyright in it.

An editor of such games may even publish a ready-to-use version of the game, if he fullfills the same restrictions (or get the copy rights from the owners of intellectual property).

Enjoy yourself, and your friends too!
