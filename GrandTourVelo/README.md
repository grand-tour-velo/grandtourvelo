# Grand Tour Vélo

[English](./README.en.md)

Ce Jeu de plateau va vous immerger dans l'atmosphère des courses cyclistes à étapes, vous permettant de mener vos courreurs à la victoire dans votre grand tour favori, en essayant de remporter les différents classements : général, aux points, de la montagne, du meilleur jeune.

Vous pourrez fabriquer vous-même le matériel, avec un peu d'habileté, et imaginer vos propres étapes, que nous vous invitons à partager (sous licence open source) en les intégrant au projet.

Les règles en Français sont disponibles dans le répertoire [FR](./FR), tandis que des cartes d'étapes sont proposées dans le répertoire [etapes](./etapes).

Le prototype du plateau, fait en 2018, vous donne un premier aperçu de ce à quoi ressemble le jeu

![](Prototype_2018.jpg)

Depuis, les règles ont été développées et amendées, et vous disposez maintenant de plusieurs cartes distinctes, une par étape.

Et voici une partie en cours sur ce prototype (règles de base) :

![](Premiere_partie.jpg)

Le jeu est mis à votre disposition sous une Licence Art Libre, ce qui vous permet de le diffuser, le modifier, l'améliorer, sans autre restriction que de ne pas utiliser d'éléments soumis à propriété intellectuelle dans ce que vous diffusez (attention aux logos de marques et maillots distinctifs, notamment).

Des éditeurs de jeux de société pourront vendre des versions prêtes à l'emploi, sous la même restriction (sauf à se procurer les droits correspondants auprès des détenteurs de la propriété).

## Vous procurer les éléments du jeu

Vous aurez besoin des règles, des pions, et de cartes d'étapes.

#### Règles

Elles sont dans le dossier [FR](./FR), sous forme PDF, en A4 pour ceux ayant une imprimante recto, et en brochure pour ceux ayant une imprimante recto-verso (pensez à "relier" sur le petit côté), qui pourront en faire un livret A5 relié.

#### Pions

Ils sont disponibles en PDF A4 ci-dessus, pouvant être imprimés (puis découpés et collés sur un support), mais aussi au format vectoriel SVG, pouvant être ouverts avec un logiciel approprié (tel que le logiciel libre [Inkscape](https://inkscape.org/fr/)) et ainsi redimensionnés à vos besoins.

#### Étapes

Plusieurs sont disponibles en PDF dans le répertoire [etapes](./etapes), dont certaines pouvant être imprimées sur 4 pages (pensez à désactiver le recto-verso de votre imprimante). Comme les pions, elles sont aussi fournies en SVG, vous permettant de les modifier ou d'en tirer d'autres cartes.

Tous ces éléments sont libres, modifiables et rediffusables, dans le respect de la Licence Art Libre qui leur est apposée.

N'hésitez pas à proposer des améliorations, et notamment de nouvelles cartes d'étapes, qui pourront être ajoutées au site si elles sont sous licence libre.

**Amusez-vous bien**