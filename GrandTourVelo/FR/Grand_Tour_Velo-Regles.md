# Grand Tour Vélo

![](../Grand_Tour_Velo.png)

*Copyleft  2018 - vmahe@free.fr - Licence Art Libre*

## Introduction

Le jeu Grand Tour Vélo est un jeu de société conçu pour simuler les grandes courses cyclistes à étapes. Cependant, la plupart des grands tours cyclistes étant des marques déposés, nous avons fait le choix d’un marquage neutre, qui laisse libre cours à
votre imagination, et nous permet de placer ce jeu sous Licence Art Libre[1](#sdfootnote1sym).

En conséquence, vous êtes libre de dupliquer ce jeu, le modifier, et distribuer la version modifiée, pour peu que vous respectiez la licence du jeu (les éventuels droits des
marques du monde cycliste et toute propriété intellectuelle associée aux grands tour cyclistes nous imposent de créer des cartes, pions et éléments de jeu tirés de notre imagination, sans lien avec le monde rél, afin de ne pas enfreindre ces droits).

Les règles du jeu proposent deux niveaux de pratique, l’une pour les enfants et les personnes souhaitant s’amuser sans efforts, la seconde pour les joueurs attendant un peu plus de difficulté et de réflexion.

Les cartes L1, M1 et CM1 sont une première proposition de plateaux de jeu définissant des parcours d'étapes en ligne, en montagne et de contre-la-montre, sommaires (je ne suis pas doué pour les représentations artistiques), et vous pouvez proposer de nouvelles cartes (tant qu'elles respectent les principes de droits d'auteur et la décence).

Bon jeu à tous !

## Principes de jeu

### Simplicité

Le Grand Tour Vélo vous propose de simuler un grand tour cycliste aussi proche de la réalité que possible, tout en assurant une action de jeu simple.

Après avoir confectionné les accessoires de jeu (voir la section ***Matériel libre et personnalisé***), vous pourrez vous lancer à l'assaut du chronomètre et des cols hors catégorie.

Le principe de base retenu est celui de l’avancée de pions à la faveur du jet d’un dé ordinaire, le résultat de ce jet (1 à 6) donnant le nombre de pas que le pion peut parcourir.

### Réalisme

Le classement de l’étape dépend du premier à franchir la ligne ; si deux joueurs franchissent la ligne dans le même jet de dé, c’est celui qui a le plus de pas après la ligne qui gagne, et le temps du second comprend les pas de différence avec le vainqueur.

Le classement est réalisé comme dans les grands tours cyclistes, vous plongeant dans l’atmosphère de ces compétitions.

Vous disposez aussi de challenges intermédiaires, donnant lieu aux classements spéciaux classiques : meilleur sprinter, meilleur grimpeur.

### Votre Tour

Le plateau de jeu prototype comportait trois grands types d’étapes classiques de ces grands tours : l’étape en ligne, l’étape de montagne et le contre-la-montre.

Vous pouvez déjà, à partir de ces trois étapes, composer une épreuve complète, telle qu’une classique d’une semaine, comprenant par exemple 3 étapes en ligne, 2 étapes de montagne et 1 contre-la-montre, ou aller plus loin et concevoir un grand tour, avec ses 21 étapes.

Vous pouvez choisir de raccourcir certaines de ces étapes, en démarrant à partir du sprint intermédiaire.

### Des cartes ouvertes

En plus d'utiliser un plateau tel que le prototype, vous pouvez aussi concevoir vos propres cartes, ou bien utiliser celles qui seront proposées par les autres joueurs.

Les plus intéressantes seront ajoutées au site Web du jeu.

### Matériel libre et personnalisé

Un peu de bricolage vous permet de réaliser votre propre matériel, en fonction des outils et matériaux dont vous disposez. La réalisation de ce matériel sera d'ailleurs le premier plaisir que vous procurera le jeu.

Pour pouvoir "planter" les pions (les faire tenir debout) sur une carte d'étape, celle-ci doit être (une fois imprimée), collée sur un support un peu épais et pas trop dur : le prototype était une planche de bois aggloméré, qui a nécessité de percer les trous des emplacements de pions, mais vous pouvez aussi utiliser un carton épais ou une plaque d'isolant. 

La nature du support sur lequel vous collerez les cartes d'étapes conditionne le choix du support des pions : un carton épais peut être associé à des pions montés sur des épingles, celles-ci pouvant percer facilement le carton, tandis que du polystyrène permettra de fixer les pions sur des demis cure-dents.

## Règles de base

Elles s’adressent aux joueurs simples (enfants, adultes ne se prenant pas la tête), et vont concerner peu d’étapes (guère plus d’une de chaque sorte).

Nombre de joueurs : 2 à 6.

Pions : les 6 pions isolés de 6 couleurs différentes.

Matériel : un dé à jouer ; soit un plateau de jeu avec les trois types d’étapes (étape en ligne, montagne, contre-la-montre), comme celui du prototype, soit un ensemble de cartes-plateau.

Les joueurs alignent leur pion derrière le kilomètre zéro, en notant le point de bonus de ceux en 2e ligne, qu’ils pourront utiliser une seule fois.

Chaque joueur lance le dé, note le chiffre résultant, et avance d’autant de pas que ce chiffre. Si là où le pion doit aller un autre pion est présent, il doit rester derrière (sur le premier emplacement libre). Si son chiffre l’envoie sur un emplacement libre, il y va, même si le chemin jusque là est bloqué.

NB : quand il y a plusieurs « voies » sur la route, le joueur peut changer de file si nécessaire. Quand il n’y a qu’une « voie » (montagne), le joueur n’ayant pas un chiffre suffisant pour « sauter » par-dessus les concurrents regroupés devant lui devra occuper le dernier emplacement libre derrière eux.

Le contre-la-montre se joue avec un départ décalé de 2 lancers entre chaque joueur, sachant que le temps passé sera calculé, pour chaque coureur, selon le même principe que les temps du classement général, en prenant ici le nombre de lancers effectués par chaque joueur, tenant compte ainsi du décalage de 2 lancers. Les dépassements se font sans bloquer les coureurs. Si le coureur rattrapant ne peut pas occuper la case qu'il devrait (cas d'un contre-la-montre en montagne), parce qu'elle est occupée par un coureur devant lui, il garde en mémoire la case et l'ajoute à son lancer suivant.

Les premiers enfants ayant joué sur le prototype ont tout de suite pris le pli de simplement additionner les points des sprints intermédiaire et final pour connaître le résultat d'une étape en ligne (et de même avec les points du meilleur grimpeur pour le classement d'un étape de montagne). Ils n'ont pas utilisé le parcours de contre-la-montre, ce type d'épreuve n'étant pas familière aux enfants.

## Règles avancées

Elles sont destinées aux joueurs plus exigeants, privilégiant la stratégie et souhaitant instiller de la tactique en contrepoint du caractère aléatoire du jet de dé.

Nombre de joueurs : 3.

Pions : les 15 pions d’équipes, de 3 couleurs distinctes, avec leurs numéros.

Matériel : un dé à jouer ; un plateau de jeu avec les trois types d’étapes (étape en ligne, montagne, contre-la-montre) ou bien des plateaux-étapes distincts.

Les joueurs préparent leurs pions, en positionnant les premiers derrière le kilomètre zéro, chacun sur une des 3 voies disponibles.

### Stratégie

#### Désignation des équipiers

Comme dans le cyclisme professionnel, chaque équipier a un rôle à joueur au sein de son équipe, et des objectifs en terme de classement.

Chaque manager (le joueur) doit viser la victoire finale aux trois grands classements, ce qui suppose d'y affecter un de leur équipier, par exemple le 1 visant le général (classement par temps), le 2 les sprints, le 3 la montagne. Comme dans le monde réel, ces objectifs sont susceptibles d'évoluer au fil des courses (et des erreurs tactiques des uns et des autres). Les équipiers 4 et 5 sont les "jeunes", ceux qui peuvent viser le classement du meilleur jeune, ce qui ne les empêche pas de concourir aux autres classements.

#### Tactiques

Chaque joueur va devoir, au cours de chaque étape, suivre et contrer les tentatives des autres équipes pour placer devant leur joueur important pour le classement à venir, et surtout quand ce joueur est susceptible de gagner des places aux différents classements...

### Séquence de jeu

Chaque joueur lance le dé une fois, note le chiffre résultant, et choisit lequel de ses pions il avance, d’autant de pas que ce chiffre.

Les deux autres joueurs lancent le dé à leur tour et décident lequel de leurs pions ils avancent.

Cette rotation est réalisée dans la séquence autant de fois qu'il y a d'équipiers, pour déplacer les neufs pions sur le plateau.

Chaque joueur doit ainsi « placer » ses différents équipiers de façon à obtenir ses buts (classement général ET classements spéciaux), tout en contrariant les objectifs des deux autres joueurs.

### Règles de déplacement

#### Voie bloquée

Si, là où le pion doit aller, un autre pion est présent, il doit rester derrière (sur le dernier emplacement libre). Si son chiffre l’envoie sur un emplacement libre, il y va, même si le chemin jusque là est bloqué. Quand il y a plusieurs « voies » sur la route, le joueur peut changer de file si nécessaire. Quand il n’y a qu’une « voie » (montagne, peloton), le joueur n’ayant pas un chiffre suffisant pour « sauter » par-dessus les concurrents regroupés devant lui devra occuper le dernier emplacement libre derrière eux.

#### Peloton et aspiration

Si un groupe de 3 coureurs est constitué, d'une longueur d'au moins 2, le coureur qui doit se déplacer au niveau de ce groupe, qu'il vienne de l'arrière ou était dans le groupe, bénéficie d'un bonus de déplacement d'une case, tant que ce bonus ne l'emmène pas au-delà du groupe.

Exemple d'un coureur X qui est à 2 cases derrière un groupe constitué des coureurs A, B et C :    o o X o A B C o o o

Si le joueur qui a X sort un dé de 4, X devrait normalement se mettre au niveau de C (en supposant qu'une seconde voie existe). Du fait de l'aspiration produite par le peloton, il bénéficie d'une case supplémentaire et peut donc placer son coureur X juste devant C.

Si un peloton de 8 coureurs est constitué, d'une longueur d'au moins 5 coureurs, le bonus est de 2 cases

#### Manque, pair et passe

Si le joueur tire un dé faible, qu'il ne souhaite pas utiliser sur l'un de ses coureurs visant un classement, et que ses autres pions ne peuvent se déplacer parce que bloqués, il doit indiquer aux autres joueurs quel pion il "joue", sachant qu'ensuite, ce pion ne pourra plus être déplacé dans la séquence de jeu en cours (il devra utiliser ses pions non encore "joués", quelques que soient les résultats de ses lancers suivants).

### Contre-la-montre

Les étapes chronométrées sont particulières en cela qu'elles sont décalées dans les séquences.

#### Contre-la-montre individuel

Les règles de base du contre-la-montre s'appliquent ici, faute d'idée sur une meilleure façon de faire. Peut-être aurez-vous mieux à proposer ; en ce cas, n'hésitez pas à envoyer vos idées.

#### Contre-la-montre par équipes

Chaque joueur aligne ses 5 équipiers au départ et va réaliser un lancer pour chacun au cours d'une même séquence, déplaçant les pions dans l'ordre qui l'arrange au mieux, sachant que les règles de dépassement et d'aspiration s'appliquent au sein de l'équipe.

Si une équipe en rattrape une autre, elle ne bénéficie pas de l'aspiration et, si elle ne peut pas utiliser tous ses points de déplacement, elle doit garder en mémoire les cases non utilisées, pour les ajouter à la séquence suivante. L'équipe rattrapée doit laisser la place de passer, en libérant quelques cases le long de sa propre équipe.

Le temps et calculé comme pour l'arrivée au classement général, mais se détermine au passage du 5e et dernier équipier à franchir la ligne. Ce temps est bien sûr appliqué ensuite à tous les membres de l'équipe.

Bon jeu à tous !

[1](#sdfootnote1anc)Licence Art Libre : [http://artlibre.org/](http://artlibre.org/)
