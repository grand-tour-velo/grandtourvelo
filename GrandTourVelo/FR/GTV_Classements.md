# Grand Tour Vélo

## Classement général

Le premier de l’étape est déterminé par le plus faible écart subsistant avant le franchissement de l'arrivée, quand plusieurs pions arrivent dans la même séquence.

Les temps sont exprimés en ticks.

Le temps du premier est calculé par son nombre de jets de dé ; chaque jet vaut 4 ticks, et on ne retient pour le dernier jet (celui qui l’a fait dépasser la ligne d'arrivée) que le nombre de pas nécessaires au franchissement.

Les temps des suivants sont déterminés en fonction du nombre de jets préliminaires à leur franchissement de la ligne (fois 4) plus les pas du dernier jet avant la ligne. En cas de franchissement à plusieurs dans le même jet, la même règle qu’entre 1er et 2e s’applique pour les distinguer.

## Classements spécifiques

### Classement par points

#### Sprints intermédiaires

- 1er : 5 points
- 2e : 3 points
- 3e : 1 point

#### Sprint final d’étape

- 1er : 20 points
- 2e : 10 points
- 3e : 5 points
- 4e : 2 point

### Classement de la montagne

#### Col de 3e catégorie

- 1er : 2 points
- 2e : 1 point

#### Col de 2e catégorie

- 1er : 5 points
- 2e : 3 points
- 3e : 1 point

#### Col de 1e catégorie

- 1er : 10 points
- 2e : 6 points
- 3e : 3 points
- 4e : 1 point

#### Col hors catégorie

- 1er : 20 points
- 2e : 12 points 
- 3e : 8 points
- 4e : 4 points
- 5e : 1 pt

#### Arrivée en altitude

Points doublés.
